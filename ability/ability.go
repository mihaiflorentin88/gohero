package ability

import (
	"math/rand"
)

type IAbility interface {
	Use(strength int, defense int) BattleResult
}

type Ability struct {
	Name       string
	Type       string
	ProcChance int
}

type Abilities struct {
	Offensive Offensive
}

func (a Ability) HasProcced() bool {
	roll := rand.Intn(10)
	if a.ProcChance < roll {
		return false
	}
	return true
}
