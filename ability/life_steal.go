package ability

import (
	"fmt"
)

type lifeSteal struct {
	Ability
}

func NewLifeSteal() *lifeSteal {
	return &lifeSteal{
		Ability{
			Name:       "LifeSteal",
			Type:       "offensive",
			ProcChance: 2,
		},
	}
}

func (a *lifeSteal) Use(strength int, defense int) BattleResult {
	result := MakeBattleResult(a.Type, a.Name)
	if !a.HasProcced() {
		return result
	}
	result.DamageDone = NewAttack().Use(strength, defense).DamageDone
	result.AttackerHealAmount = int(float64(result.DamageDone) * 0.50)
	fmt.Printf(
		"[LIFESTEAL] LifeSteal did %v damage and healed for %v.\n",
		result.DamageDone,
		result.AttackerHealAmount,
	)
	return result
}
