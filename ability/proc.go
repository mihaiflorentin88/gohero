package ability

import (
	"math/rand"
)

func CanUse(chance int, luck int) bool {
	roll := rand.Intn(10)
	chance = chance + luck/100
	if chance < roll {
		return false
	}
	return true
}
