package ability

type attackStruct struct {
	Ability
}

func NewAttack() *attackStruct {
	return &attackStruct{
		Ability{
			Name:       "Attack",
			Type:       "offensive",
			ProcChance: 2,
		},
	}
}

func (a *attackStruct) Use(strength int, defense int) BattleResult {
	attackResult := MakeBattleResult(a.Type, a.Name)
	attackResult.DamageDone = strength - defense
	return attackResult
}
