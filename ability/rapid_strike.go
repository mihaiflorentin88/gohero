package ability

import (
	"fmt"
)

type rapidStrike struct {
	Ability
}

func NewRapidStrike() *rapidStrike {
	return &rapidStrike{
		Ability{
			Name:       "RapidStrike",
			Type:       "offensive",
			ProcChance: 1,
		},
	}
}

func (a *rapidStrike) Use(strength int, defense int) BattleResult {
	result := MakeBattleResult(a.Type, a.Name)
	if !a.HasProcced() {
		return result
	}
	for i := 1; i < 5; i++ {
		result.DamageDone += NewAttack().Use(strength, defense).DamageDone
	}
	fmt.Printf(
		"[RAPIDSTRIKE] RapidStrike hit twice for a total of %v damage.\n",
		result.DamageDone,
	)
	return result
}
