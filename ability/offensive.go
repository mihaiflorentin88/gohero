package ability

type Offensive struct {
	Abilities []IAbility
}

func MakeOffensiveAbilities() []IAbility {
	return []IAbility{NewLifeSteal(), NewRapidStrike()}
}

func (o Offensive) Use(strength int, defense int) BattleResult {
	abilities := MakeOffensiveAbilities()
	result := MakeBattleResult("offensive", "nil")
	for _, ability := range abilities {
		result = ability.Use(strength, defense)
		if result.DamageDone > 0 {
			return result
		}
	}
	return result
}
