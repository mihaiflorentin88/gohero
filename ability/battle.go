package ability

type BattleResult struct {
	AbilityType        string
	AbilityName        string
	DamageDone         int
	AttackerHealAmount int
	DefenderHealAmount int
}

func MakeBattleResult(abilityType string, abilityName string) BattleResult {
	return BattleResult{
		AbilityType:        abilityType,
		AbilityName:        abilityName,
		DamageDone:         0,
		AttackerHealAmount: 0,
		DefenderHealAmount: 0,
	}
}
