module gohero

go 1.18

replace hero/main => ../main
replace hero/test => ../test
replace hero/model => ../model
replace gohero/ability => ../ability
replace gohero/ability/offensive => ../ability/offensive
