package model

import (
	"fmt"
	"gohero/ability"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type Character struct {
	Name     string
	Luck     int
	Speed    int
	Health   int
	Defense  int
	Strength int
	Log      BattleLog
}

func (character Character) Print() {
	fmt.Printf(
		"Name: %v\nLuck: %v\nSpeed: %v\nHealth: %v\nDefense: %v\nStrength: %v\n\n\n",
		character.Name,
		character.Luck,
		character.Speed,
		character.Health,
		character.Defense,
		character.Strength,
	)
}

func (attacker *Character) Attack(defender *Character) {
	abilities := ability.Abilities{Offensive: ability.Offensive{ability.MakeOffensiveAbilities()}}
	attackResult := abilities.Offensive.Use(attacker.Strength, attacker.Defense)
	if attackResult.DamageDone == 0 {
		attackResult = ability.NewAttack().Use(attacker.Strength, defender.Defense)
	}
	attacker.Log.Add(attackResult)
	attacker.Health += attackResult.AttackerHealAmount
	defenseResult := attacker.defend(attackResult)
	attacker.Health -= defenseResult.DamageDone
}

func (defender *Character) defend(attack ability.BattleResult) ability.BattleResult {
	defender.Health -= attack.DamageDone
	defender.Health += attack.DefenderHealAmount
	defenseResult := ability.MakeBattleResult("defense", "defend")
	defender.Log.Add(defenseResult)
	return defenseResult
}

func MakeCharacterName() string {
	names := [55]string{
		"Cencan",
		"Wynanne",
		"Grimcwenesc",
		"Clachell",
		"Ape",
		"Donanne",
		"Syli",
		"Vidtri",
		"Paris",
		"Ingstan",
		"Pabert",
		"Ceo",
		"Manthryth",
		"Sali",
		"Wilza",
		"Hamanne",
		"Crowgar",
		"Wig",
		"Cwentri",
		"Isum",
		"Thurinul",
		"Born",
		"El - ner",
		"Amarcír",
		"Rondrenphor",
		"Thornfan",
		"Mocír",
		"Taemel",
		"Edso",
		"Kael",
		"Maldin",
		"Thalfin",
		"Danvar",
		"Var",
		"Leonel",
		"Momo",
		"Rielthor",
		"Gardi",
		"Dáindáin",
		"Dasber",
		"Dornga",
		"Moknút",
		"Chrip",
		"Hanthud",
		"Nugad",
		"Didakmi",
		"Rihrím",
		"Hjalmag",
		"Funma",
		"Thiga",
		"Ragril",
		"Bagi",
		"Tobel",
		"Khagu",
		"Barbar",
	}
	return names[rand.Intn(len(names))]
}

func MakeCharacter() Character {
	return Character{
		Name:     MakeCharacterName(),
		Log:      MakeBattleLog(),
		Luck:     rand.Intn(4-2) + 2,
		Speed:    rand.Intn(60-40) + 40,
		Health:   rand.Intn(400-280) + 280,
		Defense:  rand.Intn(60-40) + 40,
		Strength: rand.Intn(90-60) + 60,
	}
}

func MakeCharacters(total int) []Character {
	var characters []Character
	for i := 0; i < total; i++ {
		characters = append(characters, MakeCharacter())
	}
	return characters
}
