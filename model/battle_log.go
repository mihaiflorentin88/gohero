package model

import "gohero/ability"

type BattleLog interface {
	Add(log ability.BattleResult)
}

type battleLog struct {
	Log map[string][]ability.BattleResult
}

func MakeBattleLog() BattleLog {
	battleLog := battleLog{make(map[string][]ability.BattleResult)}
	return &battleLog
}

func (bl *battleLog) Add(log ability.BattleResult) {
	bl.Log[log.AbilityType] = append(bl.Log[log.AbilityType], log)
}
