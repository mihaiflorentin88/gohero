
# The hero game

## Stats
* Health: 70 - 100
* Strength: 70 - 80
* Defence: 45 â€“ 55
* Speed: 40 â€“ 50
* Luck: 10% - 30% (0% means no luck, 100% lucky all the time).

Abilities:
* Life Steal: Heals for 20% of the damage done when procced. Thereâ€™s a n% (default = 10%) chance the attacker will use this skill every time he attacks
* Rapid strike: Strikes twice. Thereâ€™s a n% (default = 10%) chance the attacker will use this skill every time he attacks
  change heâ€™ll use this skill every time he defends.
* Magic shield: Restores 20% of the damage taken when procced. Thereâ€™s a n% (default = 23%) chance the defender will use this skill every time he takes damage.
* Thorns: Causes attackers to take 50% of the damage done when they deal damage to the defender. Thereâ€™s a n% (default = 30%) chance the defender will use this skill every time he takes damage.

### Gameplay

The heroes are initiated with random properties, within their ranges.

The first attack is done by the hero with the higher speed. If both heroes have the same speed,
then the attack is carried on by the player with the highest luck. After an attack, the heroes switch
roles: the attacker now defends and the defender now attacks.

The damage done by the attacker is calculated with the following formula:
**Damage = Attacker strength â€“ Defender defence**

The damage is subtracted from the defenderâ€™s health. An attacker can miss their hit and do no
damage if the defender gets lucky that turn.

Orderusâ€™ skills occur randomly, based on their chances, so take them into account on each turn.

### Game over
The game ends when one of the players remain without health or the number of turns reaches 20.
The application must output the results each turn: what happened, which skills were used (if any),
the damage done, defenderâ€™s health left.
If we have a winner before the maximum number of rounds is reached, he must be declared

 
-