package test

import (
	"gohero/model"
	"testing"
)

func TestMakeCharactersReturnsTwoCharacters(t *testing.T) {
	result := model.MakeCharacters(2)
	if len(result) != 2 {
		t.Fatalf("Expected model.MakeCharacters to return 2 model.Character. Got %v", len(result))
	}
}
