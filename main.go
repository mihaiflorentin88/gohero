package main

import (
	"fmt"
	"gohero/model"
	"strings"
	"time"
)

func ObjectSliceDelete(slice []model.Character, index int) []model.Character {
	copy(slice[index:], slice[index+1:])
	new := slice[:len(slice)-1]
	return new
}

func fight(attacker *model.Character, defender *model.Character) model.Character {
	attacker.Attack(defender)
	if attacker.Health <= 0 {
		return *defender
	}
	if defender.Health <= 0 {
		return *attacker
	}
	return fight(defender, attacker)
}

func prepare(characters []model.Character) []model.Character {
	if len(characters) <= 1 {
		fmt.Printf("Fight has finished! Winner is %v.\n", characters[0].Name)
		characters[0].Print()
		return characters
	}
	attacker := characters[0]
	defender := characters[1]
	fmt.Printf("%v\n", strings.Repeat("-", 100))
	fmt.Printf("Attacker: \n")
	attacker.Print()
	fmt.Printf("Defender: \n")
	defender.Print()
	winner := fight(&attacker, &defender)
	if winner.Name == attacker.Name {
		characters = ObjectSliceDelete(characters, 1)
	}
	if winner.Name == defender.Name {
		characters = ObjectSliceDelete(characters, 0)
	}
	fmt.Printf("Attacker: \n")
	attacker.Print()
	fmt.Printf("Defender: \n")
	defender.Print()
	fmt.Printf("%v\n", strings.Repeat("-", 100))
	return prepare(characters)
}

func main() {
	started_ts := time.Now().UnixMilli()
	characters := model.MakeCharacters(2)
	prepare(characters)
	fmt.Printf("Duration %v miliseconds.", time.Now().UnixMilli()-started_ts)
}
